﻿using System;
using System.IO;
using Newtonsoft.Json;
using NLog;
using SubtaskHelper.Entity;

namespace SubtaskHelper.Tool
{
    public class FileHelper
    {
        private readonly Logger _logger = LogManager.GetLogger("Json");

        public JiraConfig Deserialize(string json)
        {
            _logger.Trace($"Reading JSON file {json}");
            if (File.Exists(json))
            {
                try
                {
                    var config = JsonConvert.DeserializeObject<JiraConfig>(File.ReadAllText(json));
                    _logger.Debug("Successfully read config file");
                    return config;
                }
                catch (Exception e)
                {
                    _logger.Error($"Unable to deserialize {json}");
                    _logger.Trace(e.Message);
                    throw;
                }
            }
            else
            {
                _logger.Fatal($"Unable to find {json}");
                throw new FileNotFoundException($"Unable to find {json}");
            }
        }
    }
}