﻿using System;
using System.Collections.Generic;
using Atlassian.Jira;
using Newtonsoft.Json.Linq;
using NLog;
using SubtaskHelper.Entity;

namespace SubtaskHelper.Tool
{
    public class JiraHelper
    {
        private readonly JiraConfig _config;
        private Jira _jira;
        private readonly Logger _logger = LogManager.GetLogger("Json");

        public JiraHelper(JiraConfig config)
        {
            _config = config;
        }


        public void Auth()
        {
            try
            {
                _jira = Jira.CreateRestClient(_config.Url, _config.User, _config.Password);
                var prj = _jira.Projects.GetProjectAsync(_config.Project).Result;
                _logger.Info($"Ready to work for {prj.Name}");
            }
            catch (Exception e)
            {
                _logger.Error($"Unable to use Jira - {e.Message}");
                throw;
            }
        }


        public void Do()
        {
            Auth();

            foreach (var task in _config.Data)
            {
                Console.WriteLine();
                _logger.Info($"Working with {task.Parent}");
                _logger.Trace($"Reading all issues for {task.Parent}");
                var allSubTasks = _jira.Issues.GetIssueAsync(task.Parent).Result.GetSubTasksAsync().Result;
                foreach (var subTask in task.SubTasks)
                {
                    if (Contains(allSubTasks, subTask.Key))
                    {
                        _logger.Debug($"{task.Parent} is already has {subTask.Key}");
                    }
                    else
                    {
                        _logger.Debug(
                            $"Creating subtask {subTask.Key} with estimate {subTask.Value} for {task.Parent}");
                        var issue = _jira.CreateIssue(_config.Project, task.Parent);
                        issue.Type = "10000";
                        issue.Summary = subTask.Key;
                        issue.SaveChanges();
                        AddEstimate(issue, subTask.Value);
                    }
                }

                _logger.Info($"Done with {task.Parent}");

            }
        }

        private JToken AddEstimate(Issue issue, string estimate)
        {
            _logger.Trace($"Adding estimates for {issue.Key}");
            var editList = new List<object>
            {
                new {edit = new {originalEstimate = estimate, remainingEstimate = estimate}}
            };
            var myObject = new {update = new {timetracking = editList}};
            return _jira.RestClient.ExecuteRequestAsync(RestSharp.Method.PUT,
                $"/rest/api/2/issue/{issue.JiraIdentifier}",
                myObject).Result;
        }

        private static bool Contains(IEnumerable<Issue> subtasks, string summary)
        {
            var has = false;
            foreach (var subtask in subtasks)
            {
                if (subtask.Summary.Equals(summary, StringComparison.CurrentCultureIgnoreCase))
                {
                    has = true;
                }
            }
            return has;
        }
    }
}