﻿using System;
using System.IO;
using NLog;
using NLog.Config;
using NLog.Targets;
using SubtaskHelper.Tool;

namespace SubtaskHelper
{
    class Program
    {
        static Program()
        {
            var config = new LoggingConfiguration();
            var consoleTarget = new ColoredConsoleTarget();
            config.AddTarget("console", consoleTarget);
            consoleTarget.Layout = @"${date:format=HH\:mm\:ss} | ${level} | ${message}";
            var rule1 = new LoggingRule("*", LogLevel.Debug, consoleTarget);
            config.LoggingRules.Add(rule1);
            LogManager.Configuration = config;
        }


        static int Main(string[] args)
        {
            Console.WriteLine("Go for a cup of coffee now, Jira sub-tasks will be ready soon ^_^ \n");
            var file = Path.Combine(Directory.GetCurrentDirectory(), "jira.json");
            if (args.Length == 1)
            {
                file = args[0];
            }

            try
            {
                new JiraHelper(new FileHelper().Deserialize(file)).Do();
            }
            catch (Exception e)
            {
//                Console.WriteLine(e);
                return -1;
//                Console.WriteLine($"ooooops");
            }
            finally
            {
                Console.WriteLine("\nPress any key to quit ...");
                Console.Read();
                Console.WriteLine("By Nick :)");
           
            }
            return 0;
        }
    }
}