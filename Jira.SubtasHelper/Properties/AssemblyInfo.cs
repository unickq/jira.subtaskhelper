﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Jira.SubtaskHelper")]
[assembly: AssemblyDescription("Tool for adding subtasks to Jira")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("https://github.com/unickq")]
[assembly: AssemblyProduct("Jira.SubtaskHelper")]
[assembly: AssemblyCopyright("Copyright © Nick Chursin 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("95b45b6f-a771-48e9-a7be-6429f6626639")]
[assembly: AssemblyVersion("0.1.0.0")]
[assembly: AssemblyFileVersion("0.1.0.0")]
