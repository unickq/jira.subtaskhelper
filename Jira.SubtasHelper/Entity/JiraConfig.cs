﻿using System.Collections.Generic;

namespace SubtaskHelper.Entity
{
    public class JiraConfig
    {
        public string Url { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Project { get; set; }

        public List<IssueType> Data { get; set; }
    }
}