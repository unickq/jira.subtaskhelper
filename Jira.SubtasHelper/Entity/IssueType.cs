﻿using System.Collections.Generic;

namespace SubtaskHelper.Entity
{
    public class IssueType
    {
        public string Parent { get; set; }
        public Dictionary<string, string> SubTasks { get; set; }
    }
}